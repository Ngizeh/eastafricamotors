
# Getting started

This is just a landing Page for East Africa Motors

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/5.4/installation#installation)


Clone the repository

    git clone git@bitbucket.org:Ngizeh/EastAfricamotors.git

Switch to the repo folder

    cd EastAfricamotors

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

**TL;DR command list**

    cd east
    composer install
    cp .env.example .env
    php artisan key:generate
    php artisan serve