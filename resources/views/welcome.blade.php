<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="{{{ asset('images/eams.png') }}}">
        <title>EAM LTD.</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
     <div class="wrapper row0">
        <div id="topbar" class="hoc clear">
            <div class="fl_left">
                <ul class="faico clear">
                    <li><img src="{{asset('images/ken.png')}}" alt=""></li>
                    <li><img src="{{asset('images/tz.png')}}" alt=""></li>
                    <li><img src="{{asset('images/ug.png')}}" alt=""></li>
                    <li><img src="{{asset('images/zam.png')}}" alt=""></li>
                    <li><img src="{{asset('images/mal.png')}}" alt=""></li>
                    <li><img src="{{asset('images/sud.png')}}" alt=""></li>
                    <li><img src="{{asset('images/erit.png')}}" alt=""></li>
                    <li><img src="{{asset('images/moz.png')}}" alt=""></li>
                    <li><img src="{{asset('images/dij.png')}}" alt=""></li>
                </ul>
            </div>
            <div class="fl_right">
                <ul class="nospace inline pushright">
                    <li><i class="fa fa-user"></i> <a href="#">Register</a></li>
                    <li><i class="fa fa-sign-in"></i> <a href="#">Login</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="bgded overlay" style="background-image:url('images/kia.jpg');">
        <div class="wrapper row1">
            <header id="header" class="hoc clear">
                <div id="logo" class="fl_left">
                    <a><h1 style="font-size:1.5em">EAM LTD.</h1></a>
                </div>
            
                <nav id="mainav" class="fl_right">
                    <ul class="clear">
                        <li class="active"><a href="#">Home</a></li>
                        <li><a href="#">About</a>
                        <li><a href="#">Our Stock</a>
                        <li><a href="#">Sell your car</a></li>
                        <li><a href="#">Contact us </a></li>
                    </ul>
            </header>
        </div>
        <div id="pageintro" class="hoc clear">
            <article>
                <p>Your new experience of motoring.You’re due. Definitely due.</p>
                <h2 class="heading">EAM LTD, Home of Motoring</h2>
                <footer><a class="btn inverse" href="#">Let's Get it!</a></footer>
            </article>
        </div>
    </div>
    <div class="wrapper row3">
        <main class="hoc container clear">
            <!-- main body -->
            <div class="btmspace-80 center">
                <h3 class="nospace">Some of the Lastest Motors</h3>
                <p>Having the Latest and New products in the Market in Our Business,  Your Desire!</p>
            </div>
            <ul class="nospace group services">
                <li class="one_third first">
                    <article>
                        <div class="txtwrap">
                            <img src="{{('images/gmc.jpg')}}" alt="">
                            <h4 class="heading">GMC</h4>
                        </div>
                        <footer><a href="#">View Details</a></footer>
                    </article>
                </li>
                <li class="one_third">
                    <article>
                        <div class="txtwrap">
                            <img src="{{('images/rover.jpg')}}" alt="">
                            <h4 class="heading">Range Rover 2018</h4>
                        </div>
                        <footer><a href="#">View Details</a></footer>
                    </article>
                </li>
                <li class="one_third">
                    <article>
                        <div class="txtwrap">
                            <img src="{{('images/vehicle.png')}}" alt="">
                            <h4 class="heading">Chevrolet Spart 2018</h4>
                        </div>
                        <footer><a href="#">View Details</a></footer>
                    </article>
                </li>
            </ul>
            <!-- / main body -->
            <div class="clear"></div>
        </main>
        <div class="wrapper row5">
            <div id="copyright" class="hoc clear"> 
              <p class="center">Copyright &copy; 2006-{{date('Y')}} - All Rights Reserved - <a href="https://eastafricamotors.com/">EAM LTD.</a></p>
            </div>
          </div>
    </div>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
     <script src="{{asset('js/jquery.backtotop.js')}}"></script>
     <script src="{{asset('js/jquery.mobilemenu.js')}}"></script>
    </body>
</html>
